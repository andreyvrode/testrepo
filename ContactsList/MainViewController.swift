//
//  ViewController.swift
//  ContactsList
//
//  Created by Андрей on 04/10/2018.
//  Copyright © 2018 Andrey Fokin. All rights reserved.
//

import UIKit
import Foundation
import Contacts

class MainViewController: UIViewController {
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        var contacts = [CNContact]()
        let contactsStore = CNContactStore()
        let keys = [
            CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
            CNContactEmailAddressesKey,
            CNContactPhoneNumbersKey
        ] as [Any]
        let request = CNContactFetchRequest(keysToFetch: keys as! [CNKeyDescriptor])
        
        do {
            try contactsStore.enumerateContacts(with: request,
                                                usingBlock: { (contact, stop) in
                                                    print("Item fetched")
                                                    contacts.append(contact)
                                                    
            })
            print("\(contacts.count) contacts fetched!")
        } catch  {
            print("Contacts fetch error!")
        }
        
        
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

